/*
 * Author: Pol Alsina
 * Date: 29/03/2023
 */

using System;
using System.Drawing;

namespace P5_ClassesGeneriques
{
    /// <summary>
    /// Clase triangle fill de figura geometrica
    /// </summary>
    public class Triangle : FiguraGeometrica
    {
        private double _base;
        private double _altura;

        /// <summary>
        /// Constructor buit
        /// </summary>
        public Triangle() { }
        /// <summary>
        /// Constructor amb diferents atributs
        /// </summary>
        /// <param name="color">Conte el color de la figura geometrica</param>
        /// <param name="nom">Conte el nom</param>
        /// <param name="codi">Conte el codi de la figura geometrica</param>
        /// <param name="altura">Conte la altura del triangle</param>
        /// <param name="base">Conte la base del rectangle</param>
        public Triangle(Color color, string nom, int codi, double altura, double @base)
        {
            Color = color;
            Nom = nom;
            _altura = altura;
            _base = @base;
        }
        /// <summary>
        /// Constructor que fa una copia d'un objecte
        /// </summary>
        /// <param name="copia">Variable on conte la copia de l'objecte que volem</param>
        public Triangle(Triangle copia)
        {
            Color = copia.Color;
            Nom = copia.Nom;
            _altura = copia._altura;
            _base = copia._base;
        }
        /// <summary>
        /// Metode per poder imprimir els atributs
        /// </summary>
        /// <returns>Torna una frase amb tots els atributs de la clase</returns>
        public override string ToString()
        {
            return "Color: " + Color + " Nom: " + Nom + " Codi: " + Codi + " Base: " + _base + " Altura: " + _altura + " Area: " +
                   Area();
        }
        /// <summary>
        /// Metode que retorna la base
        /// </summary>
        /// <returns>Retorna la base</returns>
        public double GetBase()
        {
            return _base;
        }
        /// <summary>
        /// Metode que canvia la base
        /// </summary>
        /// <param name="base">Conte la nova base</param>
        public void SetBase(double @base)
        {
            _base = @base;
        }
        /// <summary>
        /// Metode que retorna la altura
        /// </summary>
        /// <returns>Retorna altura</returns>
        public double GetAltura()
        {
            return _altura;
        }
        /// <summary>
        /// Metode que canvia la altura
        /// </summary>
        /// <param name="altura">Conte la nova altura</param>
        public void SetAltura(double altura)
        {
            _altura = altura;
        }
        /// <summary>
        /// Metode que calcula l'area del rectangle
        /// </summary>
        /// <returns>Retorna l'area del rectangle</returns>
        public override double Area()
        {
            return _base * _altura / 2;
        }
        /// <summary>
        /// Comprovacio de si dos objectes son iguals
        /// </summary>
        /// <param name="obj">objecte a comprobar</param>
        /// <returns>true si son iguals</returns>
        public override bool Equals(object obj)
        {
            if (base.Equals(obj))
            {
                if (typeof(Triangle).IsInstanceOfType(obj)) return true;
            }
            return false;
        }
        /// <summary>
        /// Metode per fer comparacions
        /// </summary>
        /// <param name="x">conte l'objecte la qual s'ha de comparar</param>
        /// <returns>Retorna un numero depenent de la comparacio</returns>
        public override int CompareTo(IOrdenable x)
        {
            int num;
            var type1 = GetType();
            var type2 = x.GetType();
            if (type1 != type2)
            {
                if (type1 == typeof(Cercle))
                {
                    num = -1;
                }
                else if (type1 == typeof(Triangle))
                {
                    num = 1;
                }
                else
                {
                    //rectangle type1
                    if (type2 == typeof(Cercle))
                    {
                        num = 1;
                    }
                    else
                    {
                        num = -1;
                    }
                }
            }
            else
            {
                FiguraGeometrica typed = this;
                FiguraGeometrica typed2 = (FiguraGeometrica)x;
                if (typed.Area() < typed2.Area())
                {
                    num = -1;
                }
                else if (typed.Area() > typed2.Area())
                {
                    num = 1;
                }
                else
                {
                    num = 0;
                }
            }
            return num;
        }

        /// <summary>
        /// Metode que torna el hashcode
        /// </summary>
        /// <returns>obtencio hashcode</returns>
        public override int GetHashCode()
        {
            return base.GetHashCode() << 2 ^ Codi;
        }
    }
}