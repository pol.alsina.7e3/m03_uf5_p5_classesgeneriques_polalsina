/*
 * Author: Pol Alsina
 * Date: 29/03/2023
 */
namespace P5_ClassesGeneriques
{
    /// <summary>
    /// Clase que es filla de taula ordenable de tipus figura geometrica
    /// </summary>
    public class TaulaOrdenableFiguraGeometrica : TaulaOrdenable<FiguraGeometrica>
    {
        /// <summary>
        /// Contructor de la clase
        /// </summary>
        /// <param name="capacitat">Variable int que conte la capacitat que tindra l'array</param>
        public TaulaOrdenableFiguraGeometrica(int capacitat) : base(new FiguraGeometrica[capacitat <= 0 ? 10 : capacitat])
        {

        }
    }
}