/*
 * Author: Pol Alsina
 * Date: 29/03/2023
 */

namespace P5_ClassesGeneriques
{
    /// <summary>
    /// interficie que conte dos metode per compara diferents clases
    /// </summary>
    public interface IOrdenable
    {
        /// <summary>
        /// Metode per fer comparacions
        /// </summary>
        /// <param name="x">conte l'objecte la qual s'ha de comparar</param>
        /// <returns>Retorna un numero depenent de la comparacio</returns>
        public int CompareTo(IOrdenable x);
    }
}


