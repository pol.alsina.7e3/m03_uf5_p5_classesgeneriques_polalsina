/*
 * Author: Pol Alsina
 * Date: 08/05/2022
 */

using System;
using System.Drawing;

namespace P5_ClassesGeneriques
{
    /// <summary>
    /// Clase abtracte figura geometrica
    /// </summary>
    public abstract class FiguraGeometrica : IOrdenable
    {
        /// <summary>
        /// Codi de la figura
        /// </summary>
        protected readonly int Codi;
        /// <summary>
        /// Nom de la figura
        /// </summary>
        protected string Nom;
        /// <summary>
        /// Color de la figura
        /// </summary>
        protected Color Color;
        /// <summary>
        /// Metode abstracte que calcula la area de la figura
        /// </summary>
        /// <returns></returns>
        public abstract double Area();

        /// <summary>
        /// Constructor buit
        /// </summary>
        public FiguraGeometrica() { }
        /// <summary>
        /// Constructor amb diferents atributs
        /// </summary>
        /// <param name="nom">Conte el nom</param>
        /// <param name="color">Conte el color de la figura geometrica</param>
        /// <param name="codi">Conte el codi de la figura geometrica</param>
        public FiguraGeometrica(string nom, Color color, int codi)
        {
            Nom = nom;
            Color = color;
            Codi = codi;
        }

        /// <summary>
        /// Constructor que fa una copia d'un objecte
        /// </summary>
        /// <param name="copia">Variable on conte la copia de l'objecte que volem</param>
        public FiguraGeometrica(FiguraGeometrica copia)
        {
            Nom = copia.Nom;
            Color = copia.Color;
            Codi = copia.Codi;
        }
        /// <summary>
        /// Metode per poder imprimir els atributs
        /// </summary>
        /// <returns>Torna una frase amb tots els atributs de la clase</returns>
        public override string ToString()
        {
            return "Color:" + Color + "Nom:" + Nom + "Codi:" + Codi;
        }

        /// <summary>
        /// Metode que dona el codi
        /// </summary>
        /// <returns>Retorna el numero de codi</returns>
        public int GetCodi()
        {
            return Codi;
        }
        /// <summary>
        /// Metode que retorna el nom
        /// </summary>
        /// <returns>Retorna el nom</returns>
        public string GetNom()
        {
            return Nom;
        }
        /// <summary>
        /// Metode que canvia el nom
        /// </summary>
        /// <param name="name">Conte el nou nom</param>
        public void SetNom(string name)
        {
            Nom = name;
        }
        /// <summary>
        /// Metode que retorna color
        /// </summary>
        /// <returns>Retorna el color</returns>
        public Color GetColor()
        {
            return Color;
        }
        /// <summary>
        /// Metode que canvia el color
        /// </summary>
        /// <param name="color">Conte el nou color</param>
        public void SetColor(Color color)
        {
            Color = color;
        }
        /// <summary>
        /// Comprovacio de si dos objectes son iguals
        /// </summary>
        /// <param name="obj">objecte a comprobar</param>
        /// <returns>true si son iguals</returns>
        public override bool Equals(object obj)
        {
            if (obj == this) return true;
            if (obj == null) return false;
            if (obj.GetType() != GetType()) return false;
            return Codi.Equals(((FiguraGeometrica)obj).Codi);
        }
        /// <summary>
        /// Metode que torna el hashcode
        /// </summary>
        /// <returns>obtencio hashcode</returns>
        public override int GetHashCode()
        {
            return Codi.GetHashCode();
        }

        /// <summary>
        /// Metode per fer comparacions
        /// </summary>
        /// <param name="x">conte l'objecte la qual s'ha de comparar</param>
        /// <returns>Retorna un numero depenent de la comparacio</returns>
        public abstract int CompareTo(IOrdenable x);
    }
}