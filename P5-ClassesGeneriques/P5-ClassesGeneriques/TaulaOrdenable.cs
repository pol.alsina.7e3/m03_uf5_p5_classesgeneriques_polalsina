/*
 * Author: Pol Alsina
 * Date: 29/03/2023
 */
using System;

namespace P5_ClassesGeneriques
{
    /// <summary>
    /// Clase generica
    /// </summary>
    /// <typeparam name="T1">tipus de la clase</typeparam>
    public class TaulaOrdenable<T1> where T1 : IOrdenable
    {
        private T1[] _taula;
        private int _nreElements;

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        /// <param name="p1"></param>
        public TaulaOrdenable(T1[] p1)
        {
            _taula = p1;
        }
        /// <summary>
        /// Metode que retorna la capacitat de la taula
        /// </summary>
        /// <returns>retorna la capacitat de la taula</returns>
        public int Capacitat() { return _taula.Length; }
        /// <summary>
        /// Metode que retorna el numero d'elements de la taula
        /// </summary>
        /// <returns>retorna el numero d'elements de la taula</returns>
        public int NrElements() { return _nreElements; }
        /// <summary>
        /// Metode per afeigir valors dintre de la taula
        /// </summary>
        /// <param name="x">element a afeigir</param>
        /// <returns>retorna 0 si tot esta be</returns>
        public int Afegir(T1 x)
        {
            if (x == null) return -1;
            if (_taula.Length < _nreElements) return -2;
            _taula[_nreElements] = x;
            _nreElements++;
            return 0;
        }

        /// <summary>
        /// Metode que mostra per pantalla la posicio del element desitjat
        /// </summary>
        /// <param name="posicio">posicio la qual es busca treure l'element</param>
        /// <returns>retorna el element de la posicio desitjada</returns>
        public object ExemplarAt(int posicio)
        {
            if (posicio > _nreElements)
            {
                return null;
            }
            T1 busca = _taula[posicio];
            return busca;
        }
        /// <summary>
        /// Metode per treu un element de la taula
        /// </summary>
        /// <param name="posicio">la posicio del element a extreure de la taula</param>
        /// <returns>retorna l'element extret</returns>
        public object ExtreureAt(int posicio)
        {
            if (posicio > _nreElements)
            {
                return null;
            }
            T1 aux = _taula[posicio];
            for (int i = posicio; i < _nreElements; i++)
            {
                _taula[i] = _taula[i + 1];
            }
            _nreElements--;
            return aux;
        }
        /// <summary>
        /// Metode per buidar la taula 
        /// </summary>
        public void BuidarTaula()
        {
            for (int i = 0; i <= _nreElements; i++)
            {
                _taula[i] = default;
                _nreElements--;
            }
            Mostrar();
        }
        /// <summary>
        /// Metode que mostrar la taula
        /// </summary>
        public void Mostrar()
        {
            for (int i = 0; i < _nreElements; i++)
            {
                Console.WriteLine(_taula[i]);
            }
        }

        public void Ordenar()
        {
            IOrdenable[] aux = new IOrdenable[_nreElements];
            for (int i = 0; i < _nreElements; i++)
            {
                aux[i] = _taula[i];
            }

            Algorismes.Ordenar(ref aux, _nreElements);
            for (int i = 0; i < _nreElements; i++)
            {
                _taula[i] = (T1)aux[i];
            }
        }
        /// <summary>
        /// Metode que imprimeix per pantalla certs detalls de la taula
        /// </summary>
        /// <returns>retorna un string printat per pantalla</returns>
        public string Visualitzar()
        {
            return Capacitat().ToString() + " " + _nreElements.ToString() + " " + _taula;
        }
    }
}