/*
 * Author: Pol Alsina
 * Date: 29/03/2023
 */

namespace P5_ClassesGeneriques
{
    /// <summary>
    /// Clase que conte metodes bombolla per ordenar les diferents clases de data i figures geometriques
    /// </summary>
    public class Algorismes
    {
        /// <summary>
        /// Metode bombolla que ordena les figures geometriques
        /// </summary>
        /// <param name="obj">Array de figures geometriques a ordenar</param>
        /// <param name="nreElem">Numero de elements que conte l'array passat</param>
        public static void Ordenar(ref IOrdenable[] obj, int nreElem)
        {
            for (int x = 0; x < nreElem; x++)
            {
                for (int i = 0; i < nreElem - 1; i++)
                {
                    int indexSeguent = i + 1;

                    if (obj[i].CompareTo(obj[indexSeguent]) > 0)
                    {
                        IOrdenable temporal = obj[i];
                        obj[i] = obj[indexSeguent];
                        obj[indexSeguent] = temporal;
                    }
                    
                }
                //Console.WriteLine(obj[x].ToString());
            }
        }

    }
}