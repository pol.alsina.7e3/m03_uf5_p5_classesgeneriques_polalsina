/*
 * Author: Pol Alsina
 * Date: 29/03/2023
 */

using System;
using System.Drawing;

namespace P5_ClassesGeneriques
{
    /// <summary>
    /// Clase cercle fill de figura geometrica
    /// </summary>
    public class Cercle : FiguraGeometrica
    {
        private double _radi;

        /// <summary>
        /// Constructor buit
        /// </summary>
        public Cercle() { }
        /// <summary>
        /// Constructor amb diferents atributs
        /// </summary>
        /// <param name="color">Conte el color de la figura geometrica</param>
        /// <param name="nom">Conte el nom</param>
        /// <param name="codi">Conte el codi de la figura geometrica</param>
        /// <param name="radi">Conte el radi de la circunferencia</param>
        public Cercle(Color color, string nom, int codi, double radi)
        {
            Color = color;
            Nom = nom;
            _radi = radi;
        }
        /// <summary>
        /// Constructor que fa una copia d'un objecte
        /// </summary>
        /// <param name="copia">Variable on conte la copia de l'objecte que volem</param>
        public Cercle(Cercle copia)
        {
            Color = copia.Color;
            Nom = copia.Nom;
            _radi = copia._radi;
        }
        /// <summary>
        /// Metode per poder imprimir els atributs
        /// </summary>
        /// <returns>Torna una frase amb tots els atributs de la clase</returns>
        public override string ToString()
        {
            return "Color: " + Color + " Nom: " + Nom + " Codi: " + Codi + " Radi: " + _radi + " Area: " + Area() +
                   " Perimetre: " + Perimetre();
        }
        /// <summary>
        /// Metode que retorna el radi
        /// </summary>
        /// <returns>Retorna el radi</returns>
        public double GetRadi()
        {
            return _radi;
        }
        /// <summary>
        /// Metode que canvia el radi
        /// </summary>
        /// <param name="radi">Conte el nou radi</param>
        public void SetRadi(double radi)
        {
            _radi = radi;
        }
        /// <summary>
        /// Metode que calcula l'area del cercle
        /// </summary>
        /// <returns>Retorna l'area del cercle</returns>
        public override double Area()
        {
            return Math.PI * (_radi * _radi);
        }
        /// <summary>
        /// Metode que calcula el perimetre del cercle
        /// </summary>
        /// <returns>Retorna el perimetre del cercle</returns>
        public double Perimetre()
        {
            return 2 * Math.PI * _radi;
        }
        /// <summary>
        /// Comprovacio de si dos objectes son iguals
        /// </summary>
        /// <param name="obj">objecte a comprobar</param>
        /// <returns>true si son iguals</returns>
        public override bool Equals(object obj)
        {
            if (base.Equals(obj))
            {
                if (typeof(Cercle).IsInstanceOfType(obj)) return true;
            }
            return false;
        }
        /// <summary>
        /// Metode que torna el hashcode
        /// </summary>
        /// <returns>obtencio hashcode</returns>
        public override int GetHashCode()
        {
            return base.GetHashCode() << 2 ^ Codi;
        }

        /// <summary>
        /// Metode per fer comparacions
        /// </summary>
        /// <param name="x">conte l'objecte la qual s'ha de comparar</param>
        /// <returns>Retorna un numero depenent de la comparacio</returns>
        public override int CompareTo(IOrdenable x)
        {
            int num;
            var type1 = GetType();
            var type2 = x.GetType();
            if (type1 != type2)
            {
                if (type1 == typeof(Cercle))
                {
                    num = -1;
                }
                else if (type1 == typeof(Triangle))
                {
                    num = 1;
                }
                else
                {
                    //rectangle type1
                    if (type2 == typeof(Cercle))
                    {
                        num = 1;
                    }
                    else
                    {
                        num = -1;
                    }
                }
            }
            else
            {
                FiguraGeometrica typed = this;
                FiguraGeometrica typed2 = (FiguraGeometrica)x;
                if (typed.Area() < typed2.Area())
                {
                    num = -1;
                }
                else if (typed.Area() > typed2.Area())
                {
                    num = 1;
                }
                else
                {
                    num = 0;
                }
            }
            return num;
        }
    }
}