﻿/*
 * Author: Pol Alsina
 * Date: 29/03/2023
 */
using System;
using System.Drawing;

namespace P5_ClassesGeneriques
{
    /// <summary>
    /// Clase on es fan les diferents proves
    /// </summary>
    class ProvaTaulaOrdenable
    {
        static void Main()
        {

           var cercle1 = new Cercle(Color.Black, "cercle1", 5, 3);
           var triangle1 = new Triangle(Color.Green, "Triangle1", 777, 4, 6);
            var rectangle1 = new Rectangle(12, 5, 789, "rectangle1", Color.Red);
            var cercle2 = new Cercle(Color.Black, "cercle2", 55, 7);
            var triangle2 = new Triangle(Color.Green, "Triangle2", 747, 15, 16);
            var rectangle2 = new Rectangle(12, 8, 7439, "rectangle2", Color.Red);

            TaulaOrdenableFiguraGeometrica tofg = new TaulaOrdenableFiguraGeometrica(50);
            tofg.Afegir(cercle1);
            tofg.Afegir(triangle1);
            tofg.Afegir(rectangle1);
            tofg.Afegir(cercle2);
            tofg.Afegir(triangle2);
            tofg.Afegir(rectangle2);
            Console.WriteLine("Mostrar taula");
            tofg.Mostrar();
            Console.WriteLine("Mostrar taula Ordenanda");          
            tofg.Ordenar();
            tofg.Mostrar();
            Console.WriteLine();
            Console.WriteLine(tofg.Visualitzar());
            Console.WriteLine("Capacitat: "+tofg.Capacitat());
            Console.WriteLine("Numero de elements: "+tofg.NrElements());
            Console.WriteLine("Mostrar element selecionat");
            Console.WriteLine(tofg.ExemplarAt(1));
            Console.WriteLine("Extreure element selecionat");
            Console.WriteLine(tofg.ExtreureAt(1));
            Console.WriteLine("Mostrar taula amb element tret");
            tofg.Mostrar();
            tofg.BuidarTaula();
            Console.WriteLine("Mostrar taula buida");
            tofg.Mostrar();
        }
    }
}